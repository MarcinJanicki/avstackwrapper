# -*- coding: utf-8 -*-
"""
 This script provides wrapper for Daniel Beer's perl script avstack.pl to analyse stack usage on memory-constrained
 system which use gcc https://dlbeer.co.nz/oss/avstack.html
 @author M.J.
 @created 2020-06-11
 @version 1.0 (Python 3.7)

 @deploy works with Anaconda prompt: "pyinstaller --onefile --icon=chip.ico avstackwrapper.py"
         not possible to cross-create 32 bit version on 64 bit computer!

"""

import logging
import configparser
import argparse
import sys
import inspect
import os
import subprocess
import collections
import glob

# === definitions ===========================================================
APP_VERSION = "0.6.1.0"
AVSTACK_INFO = str("It is a simple wrapper for Daniel Beer's perl script analysing stack usage "
                   "on memory-constrained systems which use gcc https://dlbeer.co.nz/oss/avstack.html")
LOG = logging.getLogger()
Paths = collections.namedtuple("Paths", "perl, script, objdump, scriptmod")


class Error(Exception):
    """Error type connected with this wrapper"""


# === functions definitions =================================================
def main():
    """Main function of wrapper"""
    config = configparser.ConfigParser()
    config.read(os.path.join(get_script_dir(), "config.ini"))
    cmdline = get_parsed_args()
    flag_verbose = bool(cmdline.verbose or config["Log"].getboolean("verbose"))
    init_logger(config["Log"], flag_verbose)
    flag_wait_btn = config["Processing"].getboolean("wait_for_btn_close", True)

    LOG.info("AVStackWrapper %s built with Python %s\n%s\n", APP_VERSION, sys.version, AVSTACK_INFO)

    # from config.ini, find path to perl, objcopy binary
    # from commandline, get input folder
    try:
        paths = process_paths(cmdline, config)
    except Error as ex:
        return handle_error("error: {}".format(ex), 1, flag_wait_btn)

    # create modified copy of avstack.pl script with modified path to objcopy
    try:
        scriptmod = create_mod_script(paths.script, paths.objdump)
        paths = Paths._make([paths.perl, paths.script, paths.objdump, scriptmod])
    except Error as ex:
        LOG.critical("error: %s", str(ex))
    except Exception as ex:
        return handle_error("unexpected error: {}".format(ex), 2, flag_wait_btn)

    # get list of files and call perl with script and list of files as arguments
    files = get_input_files(cmdline, config)
    prog_call = [paths.perl, paths.scriptmod] + files

    try:
        logging.info("Please wait, analysing stack in progress...")
        # subprocess.run() is blocking in a way that function returns when process completes operation, for getting
        # live results with unbuffered stdout it shall be modified to subprocess.Popen(..) call
        process = subprocess.run(prog_call, check=True, universal_newlines=True, capture_output=False,
                                 stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        LOG.info("Results:\n%s", process.stdout)
        LOG.info("Legend:\n"
                 "'>' are root functions (not called from anywhere else: main, interrupts, unused)\n"
                 "'R' are recursive functions")
    except OSError as ex:
        return handle_error("Cannot run Stack Analysis script: {}".format(ex), 3, flag_wait_btn)
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 2:
            errmsg = str("Please remember that:\n"
                         "- generation of .su files need to be enabled in gcc with -fstack-usage flag"
                         "- assembler source files .s does not produce .su output (such files can be omitted using 'ignore' list)")
        else:
            errmsg = "avstack script returned {}".format(ex.returncode)
        if ex.output:
            errmsg = ex.output + "\n" + errmsg
        return handle_error("Stack analysis failed - " + errmsg, 4, flag_wait_btn)
    except Exception as ex:
        return handle_error("unexpected error: {}".format(ex), 5, flag_wait_btn)

    try:
        os.remove(scriptmod)
    except OSError as ex:
        return handle_error("Cannot remove modified script file {}".format(ex), 6, flag_wait_btn)

    if config["Processing"].getboolean("wait_for_btn_close", True):
        input("Press Enter to close application...")
    return 0


def handle_error(msg: str, code: int, wait_for_button: bool) -> int:
    """ wraps logging error message, waiting for button press and exiting with error code into one line """
    LOG.critical(msg)
    if wait_for_button:
        input("Press Enter to close application...")
    return code


def get_parsed_args():
    """ Defines and returns the given arguments to the program. """
    argparser = argparse.ArgumentParser(description="Program to analyze linker maps for memory usage")
    argparser.add_argument("-d", "--dir", required=True, type=str,
                           help="Path to folder where object (.o) and stack usage (.su) files are stored")
    argparser.add_argument("-o", "--objdump", required=False, type=str,
                           help="Path to objdump binary what should be used")
    argparser.add_argument("-v", "--verbose", action="store_true", default=False,
                           help="Prints also additional information.")
    argparser.add_argument("-i", "--ignores", required=False, type=str,
                           help="List of semicolon-separated .o files to be included in analysis")
    return argparser.parse_args()


def get_script_dir(follow_symlinks=True):
    """ fucntion returns path of directory from which script was run """
    if getattr(sys, 'frozen', False):  # py2exe, PyInstaller, cx_Freeze
        path = os.path.abspath(sys.executable)
    else:
        path = inspect.getabsfile(get_script_dir)
    if follow_symlinks:
        path = os.path.realpath(path)
    return os.path.dirname(path)


def get_input_files(cmdline, config):
    """
    Reads recursively specified directory in search of .o files,
    but only the ones which were not added to ignores list
    """
    # recursively get list of .o files in provided folder
    files = [file.replace("\\", "/") for file in glob.glob(cmdline.dir + "/**/*.o", recursive=True)]
    files_ign = []
    if cmdline.ignores:
        tmp_files = cmdline.ignores.split(';')
    else:
        tmp_files = config["Processing"].get("ignores").split(';')

    for file in tmp_files:
        if not os.path.isabs(file):
            file = os.path.join(cmdline.dir, file)
        files_ign.append(os.path.normpath(file).replace("\\", "/"))

    return [file for file in files if file not in files_ign]


def process_paths(cmdline, config):
    """ Does error checking on provided paths, make sure they are finally absolute and normalizes objcopy path """
    path_app = get_script_dir()
    path_perl = config["Paths"].get("perl")
    path_script = config["Paths"].get("script")
    path_objdump = cmdline.objdump if cmdline.objdump else config["Paths"].get("objdump")

    if not path_perl:
        raise Error("path to perl not specified!")
    path_perl = os.path.join(path_app, path_perl) if not os.path.isabs(path_perl) else path_perl
    path_perl = os.path.normpath(path_perl).replace("\\", "/")

    if not path_script:
        path_script = os.path.join(path_app, "avstack.pl")
    elif not os.path.isabs(path_script):
        path_script = os.path.join(path_app, path_script)
    path_script = os.path.normpath(path_script).replace("\\", "/")

    if not path_objdump:
        raise Error("path to objcopy not specified!")
    path_objdump = os.path.join(path_app, path_objdump) if not os.path.isabs(path_objdump) else path_objdump
    path_objdump = os.path.normpath(path_objdump)
    # cause perl doesn't like windows separators
    path_objdump = path_objdump.replace("\\", "/")
    return Paths._make([path_perl, path_script, path_objdump, ""])


def create_mod_script(path_script: str, path_objdump: str) -> str:
    """ Creates modified avstack.pl script in which the path to objdump binary was altered as requested """
    path_mod = path_script[:-3] + "_mod.pl"
    try:
        with open(path_script, "r") as fin, open(path_mod, "w") as fout:
            for line in fin:
                fout.write(line.replace('my $objdump = "avr-objdump"', 'my $objdump = "{}"'.format(path_objdump)))
    except OSError as ex:
        raise Error("failed to create updated script: {}".format(str(ex)))
    except Exception as ex:
        raise Error("unknown exception: {}".format(str(ex)))
    return path_mod


def init_logger(log_config, verbose):
    """Initializes logger object, provides output to file and to console."""
    if verbose:
        LOG.setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)

    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(message)s")
    handler.setFormatter(formatter)
    LOG.addHandler(handler)

    if log_config.getboolean("log_to_file", False):
        # create error file handler and set level to error
        log_path = log_config.get("file_path", "log.txt")
        if not os.path.isabs(log_path):
            log_path = os.path.join(get_script_dir(), log_path)
        handler = logging.FileHandler(log_path, "w", encoding=None, delay="true")
        formatter = logging.Formatter("%(asctime)s.%(msecs)03d %(name)s - %(levelname)s: %(message)s",
                                      datefmt='%Y-%m-%d %H:%M:%S')
        handler.setFormatter(formatter)
        LOG.addHandler(handler)


# ===== execution =============================================================
sys.exit(main())
