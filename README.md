# AVStackWrapper

This short document describes repository structure and purpose.
For README of the application itself, see [doc/readme](doc/readme).

Contets of this repository:

- `src`: source code of AVStackWrapper application

- `doc`: documents describing application (README, CHANGELOG, etc.)

- `build`: scripts used for building application binary

- `dist`: files to be distributed with binary:

    - `copy`: content of this folder will be directly copied into output folder with binary
	
	- other files within folder what are used by `build` scripts to prepare binary
	
- `bin`: ready binaries in versioned folders

## 3rd Party software and Licensing

This application provides wrapper for Daniel Beer's perl script avstack.pl to analyse stack usage on memory-constrained
systems which use gcc [link](https://dlbeer.co.nz/oss/avstack.html).

The AVStackWrapper itself is distributed under GPL [license](https://www.gnu.org/licenses/gpl-3.0.html). Source code is stored in [repository](https://bitbucket.org/MarcinJanicki/avstackwrapper/).

The application needs perl environment to execute script and is therefore distributed with minimal subset of [strawberry perl](http://strawberryperl.com/).
User who has own perl environment installed/configured is oif course free to use it by altering perl path in config.ini file..

Wrapper app is built with Python and executable is created with PyInstaller. Please see CHANGLEOG file to check out exact version which were used to create binaries.

App binary uses icon by [Milena Kiseleva](https://www.iconfinder.com/MilenaKiseleva) distributed under [CC3.0 license](https://creativecommons.org/licenses/by/3.0/), downloaded from [iconfinder](https://www.iconfinder.com/icons/3007545/art_artstack_media_social_social_media_stack_theartstack_icon).

For more licensing information please see `licenses` folder.