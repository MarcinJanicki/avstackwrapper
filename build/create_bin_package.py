# -*- coding: utf-8 -*-
"""
 This script creates binary package from avrstackwrapper.py script
 @author M.J.
 @created 2020-06-15
"""

import logging
import configparser
import sys
import os
import subprocess
import collections
import inspect
import datetime
from distutils import dir_util
import shutil
import zipfile

# === definitions ===========================================================
SCRIPT_VERSION = "0.2.0.0"
LOG = logging.getLogger()
PATHS = ["pyins", "repo", "dist", "doc", "spec", "bin", "work", "build", "app", "icon"]
Paths = collections.namedtuple("Paths", ', '.join(PATHS))


class Error(Exception):
    """Error type connected with this wrapper"""


# === functions definitions =================================================
def main():
    """Main function of wrapper"""
    config = configparser.ConfigParser()
    config.read(os.path.join(get_script_dir(), "config.ini"))
    init_logger(config["Log"])

    LOG.info("Binary package builder for AVStackWrapper %s\n", SCRIPT_VERSION)

    # from config.ini get all necessary paths
    try:
        paths = process_paths(config)
    except Error as ex:
        return handle_error("error reading paths: {}".format(ex), 1)

    # get version from script
    try:
        version = get_avstack_version(paths.app)
    except Error as ex:
        return handle_error("error getting version: {}".format(ex), 2)

    # create output folder
    LOG.info("Creating output folder...")
    str_timestamp = str(datetime.datetime.now().strftime("%Y-%m-%d_%H.%M.%S"))
    path_outbin = os.path.join(paths.bin, "AVStackWrapper_Win_x64_{}_{}".format(version, str_timestamp))
    try:
        os.makedirs(path_outbin, exist_ok=True)
    except OSError as ex:
        return handle_error("unable to create output dir {}, error: {}".format(path_outbin, ex), 3)
    LOG.info("output folder %s created", path_outbin)

    # run PyInstaller
    # pyinstaller --onefile --icon ICON --specpath spec --distpath ../bin/build_out --workpath ../bin/build_tmp ../src/avstackanalyser.py
    prog_call = [paths.pyins, "--onefile", "--specpath", paths.spec, "--distpath", paths.build, "--icon", paths.icon,
                 "--workpath", paths.work, paths.app]
    try:
        logging.info("Please wait, now running PyInstaller...")
        # subprocess.run() is blocking in a way that function returns when process completes operation, for getting
        # live results with unbuffered stdout it shall be modified to subprocess.Popen(..) call
        process = subprocess.run(prog_call, check=True, universal_newlines=True, capture_output=False,
                                 stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        LOG.info("Results:\n%s", process.stdout)
    except OSError as ex:
        return handle_error("Cannot run PyInstaller: {}".format(ex), 4)
    except subprocess.CalledProcessError as ex:
        errmsg = "returned value = {}".format(ex.returncode)
        if ex.output:
            errmsg = errmsg + "\n" + ex.output
        return handle_error("PyInstaller failed - " + errmsg, 5)
    except Exception as ex:
        return handle_error("unexpected error: {}".format(ex), 6)

    LOG.info("Copying dist files...")
    try:
        dir_util.copy_tree(paths.dist, path_outbin)
    except Exception as ex:
        return handle_error("unable to copy dist files to output dir: {}".format(ex), 7)

    LOG.info("Copying binary and docs...")
    try:
        files = [os.path.join(paths.build, "avstackwrapper.exe"), os.path.join(paths.doc, "README.md"),
                 os.path.join(paths.doc, "CHANGELOG.md")]
        for file in files:
            shutil.copy2(file, path_outbin)
    except Exception as ex:
        return handle_error("unable to copy file {} to outdir: {}".format(file, ex), 8)

    LOG.info("Creating zip archive...")
    os.chdir(os.path.dirname(path_outbin))
    try:
        with zipfile.ZipFile(path_outbin + '.zip', "w", compression=zipfile.ZIP_DEFLATED, allowZip64=True) as zf:
            for root, _, filenames in os.walk(os.path.basename(path_outbin)):
                for name in filenames:
                    name = os.path.join(root, name)
                    name = os.path.normpath(name)
                    zf.write(name, name)
    except (OSError, zipfile.BadZipFile) as ex:
        return handle_error("Zipping failed: {}".format(ex), 9)

    LOG.info("Build successful.")
    input("Press Enter to close application...")
    return 0


def handle_error(msg: str, code: int) -> int:
    """ wraps logging error message, waiting for button press and exiting with error code into one line """
    LOG.critical(msg)
    input("Press Enter to close application...")
    return code


def get_script_dir(follow_symlinks=True):
    """ fucntion returns path of directory from which script was run """
    if getattr(sys, 'frozen', False):  # py2exe, PyInstaller, cx_Freeze
        path = os.path.abspath(sys.executable)
    else:
        path = inspect.getabsfile(get_script_dir)
    if follow_symlinks:
        path = os.path.realpath(path)
    return os.path.dirname(path)


def process_paths(config):
    """ Does error checking on provided paths, make sure they are finally absolute and normalizes objcopy path """
    path_root = os.path.dirname(get_script_dir())
    paths = []

    for name in PATHS:
        try:
            path = config["Paths"].get(name)
        except KeyError:
            raise Error("no 'Paths' section in config file")
        if path is not None:
            if not os.path.isabs(path):
                path = os.path.join(path_root, path)
            path = os.path.normpath(path).replace("\\", "/")
        paths.append(path)

    return Paths._make(paths)


def get_avstack_version(path: str) -> str:
    """ reads version of avstackwrapper script """
    try:
        with open(path, 'r') as fin:
            for line in fin:
                if line.startswith("APP_VERSION"):
                    values = line.split(" = ")
                    if len(values) == 2:
                        return values[1].replace('"', '').strip()
                    raise Error("unexpected format of line with 'APP_VERSION'")
    except OSError as ex:
        raise Error("opening file error: {}".format(ex))

    raise Error("'APP_VERSION' string not fouund in script")


def init_logger(log_config):
    """Initializes logger object, provides output to file and to console."""
    if log_config.getboolean("verbose", False):
        LOG.setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)

    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(message)s")
    handler.setFormatter(formatter)
    LOG.addHandler(handler)

    if log_config.getboolean("log_to_file", False):
        # create error file handler and set level to error
        log_path = log_config.get("file_path", "log.txt")
        if not os.path.isabs(log_path):
            log_path = os.path.join(get_script_dir(), log_path)
        handler = logging.FileHandler(log_path, "w", encoding=None, delay="true")
        formatter = logging.Formatter("%(asctime)s.%(msecs)03d %(name)s - %(levelname)s: %(message)s",
                                      datefmt='%Y-%m-%d %H:%M:%S')
        handler.setFormatter(formatter)
        LOG.addHandler(handler)


# ===== execution =============================================================
sys.exit(main())
