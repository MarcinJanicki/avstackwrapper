# AVStackWrapper

AVStackWrapper (c) Marcin Janicki, 2020.

This application provides wrapper for Daniel Beer's perl script avstack.pl to analyse stack usage on memory-constrained
systems which use gcc [link](https://dlbeer.co.nz/oss/avstack.html).

The AVStackWrapper itself is distributed under GPL [license](https://www.gnu.org/licenses/gpl-3.0.html). Source code is stored in [repository](https://bitbucket.org/MarcinJanicki/avstackwrapper/).

The application needs perl environment to execute script and is therefore distributed with minimal subset of [strawberry perl](http://strawberryperl.com/).
User who has own perl environment installed/configured is oif course free to use it by altering perl path in config.ini file..

Wrapper app is built with Python and executable is created with PyInstaller. Please see CHANGLEOG file to check out exact version which were used to create binaries.

App binary uses icon by [Milena Kiseleva](https://www.iconfinder.com/MilenaKiseleva) distributed under [CC3.0 license](https://creativecommons.org/licenses/by/3.0/), downloaded from [iconfinder](https://www.iconfinder.com/icons/3007545/art_artstack_media_social_social_media_stack_theartstack_icon).

For more licensing information please see `licenses` folder.

## Usage

### Command line

Call from command line AVStackWrapper with params:

- `-d --dir`: path to folder with source files to be analysed, should contain `.o` object files and `.su` stack usage files (built by gcc with flag `-fstack-usage`)

- `-o --objdump`: path to `objdump` gcc file (usually included in toolchain, i.e. `avr-objdump.exe`, `arm-none-eabi-objdump.exe`)

- `-i --ignores`: list of semicolon-separated .o files to be ignored in analysis (i.e. files are created directly from assembler and there is no .su information for them). Ignore files can be specified as direct path or path relatively to `input` folder

- `-v --verbose`: generate additional output

### Configuration file

Following fields are used from provided `config.ini` file:

- `Log` group:

  - `verbose`: boolean, when set to true, logging level is changed from `info` to `debug`
  
  - `log_to_file`: boolean, when set, console output is duplicated to file
  
  - `file_path`: string, path to logfile to be used if `log_to_file` flag is set
  
- `Paths` group:

  - `perl`: string, path to perl binary, could be just `perl` or `perl.exe` if it is included in PATH  system variable
  
  - `objcopy`: string, path to `objcopy` binary if not provided through command line arguments (command line has higher priority, `config.ini` is fallback value)
  
  - `script`: string, path to script `avstack.pl` (if there is a need to override default value meaning same folder as AVStackWrapper).

- `Processing` group:
  
  - `wait_for_btn_close`: boolean, if set, then after processing app is not closed until Enter button pressed
  
  - `ignores`: string, same as command line option (command line has higher priority, `config.ini` is fallback value)
