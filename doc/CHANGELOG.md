# Changelog

`0.6.0.0 - 2020-06-15` using `Python 3.7.6 (default, Jan  8 2020, 20:23:39)[MSC v.1916 64 bit (AMD64)]` and `PyInstaller 3.6`
- added possibility to specify ignored files also in cmdline

`0.5.0.0 - 2020-06-15` using `Python 3.7.6 (default, Jan  8 2020, 20:23:39)[MSC v.1916 64 bit (AMD64)]` and `PyInstaller 3.6`
- initial release, implemented basic functions